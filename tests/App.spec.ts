import { setActivePinia, createPinia, storeToRefs } from 'pinia'
import { useCounter } from '@store/counter'

describe('App.vue', () => {
  beforeEach(() => {
    // creates a fresh store and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(store)`
    const pinia = createPinia()
    setActivePinia(pinia)
  })

  it('Test Pinia', () => {
    const { count } = storeToRefs(useCounter())
    expect(count.value).toBe(0)
    count.value++
    expect(count.value).toBe(1)
  })
})
