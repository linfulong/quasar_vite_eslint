import { defineConfig, Alias, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { readdirSync, readFileSync, writeFileSync, lstatSync } from 'fs'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

// 将src目录和子级目录生成别名配置
function generateAlias(): Alias[] {
  const prefix = '@'
  const alias: Alias[] = [
    { find: `${prefix}app`, replacement: resolve(__dirname, './') },
    { find: `${prefix}src`, replacement: resolve(__dirname, './src') },
  ]
  const paths = readdirSync('./src')
  for (let i = 0; i < paths.length; i++) {
    const p = `./src/${paths[i]}`
    if (lstatSync(p).isFile()) {
      continue
    }

    alias.push({
      find: `${prefix}${paths[i]}`,
      replacement: resolve(__dirname, p),
    })
  }

  return alias
}

// 生成别名配置,tsconfig.json需要配置
const alias = generateAlias()
const aliasMap = alias
  .map((item) => {
    return {
      [item.find + '/*']: [item.replacement.replace(__dirname, '.') + '/*'],
    }
  })
  .reduce((p, c) => Object.assign(p, c), {})

//修改tsconfig.json的配置,添加上别名配置json
const tsconfig = readFileSync(resolve(__dirname, 'tsconfig.json')).toString()
const tsconfigMap = JSON.parse(tsconfig)
Object.assign(tsconfigMap.compilerOptions, {
  baseUrl: '.',
  paths: aliasMap,
})
writeFileSync('tsconfig.json', JSON.stringify(tsconfigMap, null, 2))

// 生成接口代理对象
const wrapProxyApi = (baseUrl: string, proxyPath: string) => {
  const reg = new RegExp(`^${proxyPath}`)
  return {
    [proxyPath]: {
      target: baseUrl,
      changeOrigin: true,
      rewrite: (p) => p.replace(reg, ''),
    },
  }
}

// https://vitejs.dev/config/
export default ({ mode }) => {
  // 加载env配置文件,源代码中需要在import.meta.env.xxxx使用
  const viteEnv = loadEnv(mode, process.cwd())

  const { VITE_SERVER_PROTOCOL, VITE_SERVER_HOSTNAME, VITE_SERVER_PORT } =
    viteEnv
  const baseUrl = `${VITE_SERVER_PROTOCOL}://${VITE_SERVER_HOSTNAME}:${VITE_SERVER_PORT}`

  return defineConfig({
    plugins: [
      vue({
        template: { transformAssetUrls },
      }),

      quasar({
        sassVariables: 'src/quasar-variables.scss',
      }),
    ],
    resolve: {
      alias: [...alias],
    },
    server: {
      host: true,
      proxy: {
        ...wrapProxyApi(baseUrl, '/api'),
      },
    },
  })
}
