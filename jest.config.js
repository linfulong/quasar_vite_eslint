/* eslint-disable */
/* @ts-nocheck */
const { readdirSync, lstatSync } = require('fs')
const { resolve } = require('path')

function generateModuleNameMapper() {
  const prefix = '@'
  const Mapper = {
    [`^${prefix}app/(.*)$`]: '<rootDir>/$1',
    [`^${prefix}src/(.*)$`]: '<rootDir>/src/$1',
  }

  const paths = readdirSync(resolve(__dirname, './src'))

  for (let i = 0; i < paths.length; i++) {
    const p = `src/${paths[i]}`
    if (lstatSync(p).isFile()) {
      continue
    }

    Mapper[`^${prefix}${paths[i]}/(.*)$`] = `<rootDir>/${p}/$1`
  }

  return Mapper
}

/** @type {import('@ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  rootDir: './',
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  // Test are actually running in node env so we need to add custom export conditons
  // https://github.com/vuejs/test-utils/issues/234#issuecomment-1133672109
  testEnvironmentOptions: {
    customExportConditions: ['node', 'node-addons'],
  },
  transform: {
    '^.+\\.vue$': '@vue/vue3-jest',
    // Jest doesn't handle non JavaScript assets by default.
    // https://github.com/vuejs/vue-cli/blob/dev/packages/%40vue/cli-plugin-unit-jest/presets/default/jest-preset.js#L19
    '.+\\.(css|styl|less|sass|scss|jpg|jpeg|png|svg|gif|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|avif)$':
      require.resolve('jest-transform-stub'),
    // '^.+\\js$': 'babel-jest',
    // '^.+\\ts$': 'ts-jest',
  },
  moduleFileExtensions: ['json', 'js', 'jsx', 'ts', 'tsx', 'vue'],
  moduleNameMapper: {
    ...generateModuleNameMapper(),
    // '^@app/(.*)$': '<rootDir>/$1',
    // '^@src/(.*)$': '<rootDir>/src/$1',
    // '^@components/(.*)$': '<rootDir>/src/components/$1',
    // '^@assets/(.*)$': '<rootDir>/src/assets/$1',
  },
  globals: {
    'ts-jest': {
      tsconfig: './tsconfig.json',
      // useESM: true,
      // babelConfig: true,
    },
  },
  // collectCoverage: true,
  // collectCoverageFrom: [
  //   'src/**/*.{js,jsx,ts,tsx,vue}',
  //   // do not cover types declarations
  //   '!src/**/*.d.ts',
  //   // do not cover main.ts because it is not testable
  //   '!src/main.ts',
  // ],
}
