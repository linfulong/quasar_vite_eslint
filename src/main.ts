import { createApp } from 'vue'
import { Quasar, Dialog, Notify } from 'quasar'

// Import icon libraries
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'
import '@quasar/extras/mdi-v6/mdi-v6.css'

// Import Quasar css
import 'quasar/src/css/index.sass'

import App from './App.vue'
import { createPinia } from 'pinia'
import { router } from './router'

// Tailwindcss
import './tailwind.css'

// common scss
import './index.scss'

const app = createApp(App)

app.use(Quasar, {
  plugins: {
    Dialog,
    Notify,
  }, // import Quasar plugins and add here
})
app.use(router)

app.use(createPinia())

// mount app to dom
app.mount('#app')
