import dayjs, { ManipulateType } from 'dayjs'
import utc from 'dayjs/plugin/utc'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

dayjs.extend(utc)
dayjs.extend(isSameOrAfter)
dayjs.extend(isSameOrBefore)

export { dayjs }
export const DateMask = 'YYYY-MM-DD'
export const TimeMask = 'HH:mm:ss'
export const Mask = `${DateMask} ${TimeMask}`

export function localTime(
  t: dayjs.ConfigType = new Date(),
  mask = Mask
): string {
  return dayjs(t).format(mask)
}

export function utcTime(t: dayjs.ConfigType = new Date(), mask = Mask): string {
  return dayjs.utc(t).format(mask)
}

// 从UTC时间转换为本地时间
export function toLocalTime(t: dayjs.ConfigType, mask = Mask): string {
  return dayjs.utc(t).local().format(mask)
}

// 从本地时间转换为UTC时间
export function toUtcTime(t: dayjs.ConfigType, mask = Mask): string {
  return dayjs(t).utc().format(mask)
}

export function getTimeString(
  t: dayjs.ConfigType = new Date(),
  isUtc = false
): string {
  if (isUtc) {
    return utcTime(t, TimeMask)
  }
  return localTime(t, TimeMask)
}

export function getDateString(
  t: dayjs.ConfigType = new Date(),
  isUtc = false
): string {
  if (isUtc) {
    return utcTime(t, DateMask)
  }
  return localTime(t, DateMask)
}

// 格式化时间
export function formatDayjs(t: dayjs.ConfigType, mask = Mask): string {
  return dayjs(t).format(mask)
}

// 添加指定单位的时间值
export function addDateTime(
  t: dayjs.ConfigType = new Date(),
  value: number,
  unit: ManipulateType = 'hour',
  isUtc = false
): dayjs.Dayjs {
  let d = dayjs(t)
  if (isUtc) {
    d = dayjs.utc(t)
  }

  return d.add(value, unit)
}

// 送去指定单位的时间值
export function subtractDateTime(
  t: dayjs.ConfigType = new Date(),
  value: number,
  unit: ManipulateType = 'hour',
  isUtc = false
): dayjs.Dayjs {
  let d = dayjs(t)
  if (isUtc) {
    d = dayjs.utc(t)
  }

  return d.subtract(value, unit)
}
