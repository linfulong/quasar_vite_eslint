import Base64 from 'crypto-js/enc-base64'
import WordArray from 'crypto-js/lib-typedarrays'
import utf8 from 'crypto-js/enc-utf8'
// import sha256 from "crypto-js/sha256";
// import encHex from "crypto-js/enc-hex";

export function wordArray2Uint8Array(wordArray: WordArray): Uint8Array {
  // Shortcuts
  const words = wordArray.words
  const sigBytes = wordArray.sigBytes

  // Convert
  // var hexChars = []
  const res = new Uint8Array(sigBytes)
  for (let i = 0; i < sigBytes; i++) {
    // hexChars.push((bite >>> 4))
    // hexChars.push((bite & 0x0f))
    res[i] = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff
  }

  return res
}

export function string2Uint8Array(encode: string): Uint8Array {
  return Uint8Array.from(encode, (s) => s.charCodeAt(0))
}

export function uint8Array2String(decode: Uint8Array): string {
  let str = ''
  decode.forEach((el) => (str += String.fromCharCode(el)))
  return str
}

export function toBase64(bytes: Uint8Array): string {
  // @ts-ignore
  const word = WordArray.create(bytes.buffer)
  return word.toString(Base64)
}

/**
 * 字符串转换成base64
 * @param {string} str 字符串
 * @returns {string} base64编码字符串
 */
export function str2Base64(str: string): string {
  return utf8.parse(str).toString(Base64)
}

/**
 * 将base64转换成字符串
 * @param {string} bs64 base64字符串
 * @returns {string} 字符串
 */
export function base642Str(bs64: string): string {
  return utf8.stringify(Base64.parse(bs64))
}
