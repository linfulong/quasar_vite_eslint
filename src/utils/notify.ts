import { Notify, QNotifyCreateOptions, QNotifyUpdateOptions } from 'quasar'

// 设置Quasar通知插件默认参数
Notify.setDefaults({
  position: 'top',
  timeout: 2500,
  textColor: 'white',
  message: 'Default notify',
  actions: [{ icon: 'close', color: 'white', dense: true, rounded: true }],
})

export type NotifyResponse = (props?: QNotifyUpdateOptions) => void

export enum NotifyType {
  positive = 'positive',
  negative = 'negative',
  warning = 'warning',
  info = 'info',
  ongoing = 'ongoing',
}

export enum NotifyColor {
  primary = 'primary',
  secondary = 'secondary',
  accent = 'accent',
  dark = 'dark',
  positive = 'positive',
  negative = 'negative',
  info = 'info',
  warning = 'warning',
}

// 成功类型消息提示
export function successMessage(
  msg: string,
  option?: QNotifyCreateOptions
): NotifyResponse {
  return Notify.create({
    // icon: 'check_circle',
    ...option,
    message: msg,
    type: NotifyType.positive,
  })
}

// 一般类型消息提示
export function infoMessage(
  msg: string,
  option?: QNotifyCreateOptions
): NotifyResponse {
  return Notify.create({
    ...option,
    message: msg,
    type: NotifyType.info,
  })
}

// 警告类型消息提示
export function warnMessage(
  msg: string,
  option?: QNotifyCreateOptions
): NotifyResponse {
  return Notify.create({
    ...option,
    message: msg,
    type: NotifyType.warning,
  })
}

// 错误类型消息提示
export function errorMessage(
  msg: string,
  option?: QNotifyCreateOptions
): NotifyResponse {
  return Notify.create({
    // icon: 'error',
    ...option,
    message: msg,
    type: NotifyType.negative,
  })
}

export interface ongoingOperation {
  success(tips?: string): void

  failed(tips?: string): void
}

// 持续的消息，有动态加载的动画，返回关闭消息的方法操作对象
export function ongoingMessage(
  msg: string,
  option?: QNotifyCreateOptions
): ongoingOperation {
  const _notify = Notify.create({
    ...option,
    message: msg,
    type: NotifyType.ongoing,
  })

  const success = (tips?: string) => {
    _notify({
      type: NotifyType.positive,
      message: tips ?? msg,
    })
  }
  const failed = (tips?: string) => {
    _notify({
      type: NotifyType.negative,
      message: tips ?? msg,
    })
  }

  return { success, failed }
}
