import { RouteRecordRaw } from 'vue-router'

const pages = import.meta.glob('../pages/*.vue')
export const mainRouteChildren: RouteRecordRaw[] = Object.keys(pages).map(
  (key) => {
    const name = key.split('/').pop()?.replace('.vue', '')

    return {
      path: name,
      name: name,
      component: pages[key],
    } as RouteRecordRaw
  }
)

export const LoginRouteName = 'Login'

const routes: Readonly<RouteRecordRaw[]> = [
  // {
  //   path: "/login",
  //   name: LoginRouteName,
  //   component: () => import("../layouts/Login.vue"),
  // },
  {
    path: '/',
    name: 'main',
    component: () => import('../layouts/Layout.vue'),
    children: [
      ...mainRouteChildren,
      {
        path: '',
        redirect:
          (mainRouteChildren[0]?.name as string | undefined) ?? 'Error404',
        name: 'redirect_Home',
      },
    ],
  },
  {
    path: '/:catchAll(.*)*',
    name: 'Error404',
    component: () => import('../layouts/Error404.vue'),
  },
]

export { routes }
